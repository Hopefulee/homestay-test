(function () {
  'use strict';

  angular.module('vendingMachine').controller('MainController', mainController);

  mainController.$inject = ['drinkService'];
  function mainController(drinkService) {
    var vm = this;
    vm.title = 'Vending machine';
    vm.cash = 0;
    vm.creditCard = 0;
    vm.selectedDrink;
    vm.drinkList = [];
    vm.locked = true;
    vm.lastRestockQuantity = 0;
    vm.soldCan = 0;

    vm.onSelectDrink = onSelectDrink;
    vm.selectPurchaseByCash = selectPurchaseByCash;
    vm.selectPurchaseByCreditCard = selectPurchaseByCreditCard;
    vm.restock = restock;
    vm.getDrinkList = getDrinkList;

    vm.getDrinkList();

    function getDrinkList() {
      vm.drinkList = drinkService.getDrinks();
    }

    function onSelectDrink(selectedDrink) {
      vm.selectedDrink = selectedDrink;
    }

    function selectPurchaseByCash(selectedDrink) {
      selectedDrink.quantity -= 1;
      vm.cash += selectedDrink.price;
      vm.soldCan += 1;
      vm.selectedDrink = null;
    }

    function selectPurchaseByCreditCard(selectedDrink) {
      selectedDrink.quantity -= 1;
      vm.creditCard += selectedDrink.price;
      vm.soldCan += 1;
      vm.selectedDrink = null;
    }

    function restock() {
      vm.lastRestockQuantity = 0;
      vm.drinkList.forEach(function (drink) {
        vm.lastRestockQuantity += (10 - drink.quantity);
        drink.quantity = 10;
      });
      vm.cash = 0;
      vm.creditCard = 0;
      vm.soldCan = 0;
    }
  }
})();
