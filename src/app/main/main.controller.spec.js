(function() {
  'use strict';
  var drinkService, vmScope;

  describe('Main controllers', function(){
    var vm, $rootScope;
    beforeEach(module('vendingMachine'));

    beforeEach(function () {
      module(function () {
      });

      inject(function ($injector, _$controller_) {
        $rootScope = $injector.get('$rootScope');
        drinkService = $injector.get('drinkService');

        vmScope = $rootScope.$new();
        vm = _$controller_('MainController', { '$scope': vmScope });
      });
    });

    describe('Get drink list', function () {
      var data = [
        {
          title: 'Coke',
          description: 'Coke',
          price: 1.50,
          quantity: 10,
          image: 'coke.jpg'
        },
        {
          title: 'Coke Zero',
          description: 'Coke Zero',
          price: 1.50,
          quantity: 10,
          image: 'coke-zero.jpg'
        }];
      beforeEach(function () {
        spyOn(drinkService, 'getDrinks').and.returnValue(data);
        vm.getDrinkList();
      });
      it('should call getDrink from drinkService', function () {
        expect(vm.drinkList).toEqual(data);
      });
    });

    describe('select drink', function () {
      var selectedDrink = { quantity: 10, title: 'drink 1'};
      beforeEach(function () {
        vm.onSelectDrink(selectedDrink);
      });
      it('should have correct selected drink', function () {
        expect(vm.selectedDrink).toEqual(selectedDrink);
      });
    });

    describe('Purchase by cash', function () {
      var selectedDrinkParam = {
        title: 'Coke',
        description: 'Coke',
        price: 1.50,
        quantity: 10,
        image: 'coke.jpg'
      };
      var currectCash = 0;
      var currectSoldCan = 0;
      beforeEach(function () {
        vm.cash = 0;
        vm.soldCan = 0;
        vm.selectedDrink = {
          title: 'Coke',
          description: 'Coke',
          price: 1.50,
          quantity: 10,
          image: 'coke.jpg'
        }
        vm.selectPurchaseByCash(selectedDrinkParam);
      });
      it('should correct cash', function () {
        currectCash += selectedDrinkParam.price;
        expect(vm.cash).toEqual(currectCash);
      });
      it('should correct sold can', function () {
        currectSoldCan += 1;
        expect(vm.soldCan).toEqual(currectSoldCan);
      });
      it('should un-select drink', function () {
        expect(vm.selectedDrink).toEqual(null);
      });
    });

    describe('Purchase by credit card', function () {
      var selectedDrinkParam = {
        title: 'Coke',
        description: 'Coke',
        price: 1.50,
        quantity: 10,
        image: 'coke.jpg'
      };
      var currectCreditCard = 0;
      var currectSoldCan = 0;
      beforeEach(function () {
        vm.cash = 0;
        vm.soldCan = 0;
        vm.selectedDrink = {
          title: 'Coke',
          description: 'Coke',
          price: 1.50,
          quantity: 10,
          image: 'coke.jpg'
        }
        vm.selectPurchaseByCreditCard(selectedDrinkParam);
      });
      it('should correct cash', function () {
        currectCreditCard += selectedDrinkParam.price;
        expect(vm.creditCard).toEqual(currectCreditCard);
      });
      it('should correct sold can', function () {
        currectSoldCan += 1;
        expect(vm.soldCan).toEqual(currectSoldCan);
      });
      it('should un-select drink', function () {
        expect(vm.selectedDrink).toEqual(null);
      });
    });

    describe('restock', function () {
      beforeEach(function () {
        vm.drinkList = [
          {
            title: 'Coke',
            description: 'Coke',
            price: 1.50,
            quantity: 9,
            image: 'coke.jpg'
          },
          {
            title: 'Coke Zero',
            description: 'Coke Zero',
            price: 1.50,
            quantity: 8,
            image: 'coke-zero.jpg'
          }];
        vm.restock();
      });
      it('should have correct restock amount', function () {
        expect(vm.lastRestockQuantity).toEqual(3);
      });
      it('should reset cash', function () {
        expect(vm.cash).toEqual(0);
      });
      it('should reset creditCard', function () {
        expect(vm.creditCard).toEqual(0);
      });
      it('should reset soldCan', function () {
        expect(vm.soldCan).toEqual(0);
      });
      it('should refill all stocks to 10', function () {
        expect(vm.drinkList).toEqual([
          {
            title: 'Coke',
            description: 'Coke',
            price: 1.50,
            quantity: 10,
            image: 'coke.jpg'
          },
          {
            title: 'Coke Zero',
            description: 'Coke Zero',
            price: 1.50,
            quantity: 10,
            image: 'coke-zero.jpg'
          }]);
      });

    });
  });
})();
