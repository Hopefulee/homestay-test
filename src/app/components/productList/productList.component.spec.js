describe('Product list component', function () {
  var component, rootScope;

  beforeEach(module('vendingMachine'));
  beforeEach(function () {
    module(function () {
    });

    inject(function ($injector, _$componentController_) {
      rootScope = $injector.get('$rootScope');

      component = _$componentController_('productList',
        {'$scope': rootScope.$new()},
        {
          onSelectedDrink: jasmine.createSpy('onSelectedDrink')
        }
      );
    });
  });

  describe('Select drink', function () {
    var drinkItem = { quantity: 10, title: 'drink 1'};
    beforeEach(function () {
      component.selectedDrink(drinkItem);
    });
    it('should call purchaseByCash', function () {
      expect(component.onSelectedDrink).toHaveBeenCalledWith({selectedDrink: drinkItem});
    });
  });
});
