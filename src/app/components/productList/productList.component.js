(function () {
  'use strict';

  angular.module('vendingMachine').component('productList', {
    templateUrl: 'app/components/productList/productList.component.html',
    controller: ProductList,
    controllerAs: 'productListVm',
    bindings: {
      drinkList: '<',
      locked: '<',
      onSelectedDrink: '&'
    }
  });

  ProductList.$inject = [];

  function ProductList() {
    var productListVm = this;
    productListVm.selectedDrink = selectedDrink;

    function selectedDrink(selectedDrink) {
      productListVm.onSelectedDrink({selectedDrink: selectedDrink});
    }
  }
})();
