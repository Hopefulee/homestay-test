(function () {
	'use strict';

	angular.module('vendingMachine').component('productItem', {
		templateUrl: 'app/components/productItem/productItem.component.html',
		controller: ProductItem,
		controllerAs: 'productItemVm',
    bindings: {
      drink: '<',
      lock: '<',
      onSelectedDrink: '&'
    }
	});

	ProductItem.$inject = [];

	function ProductItem() {
    var productItemVm = this;
    productItemVm.productImage = getProductImageUrl();
    productItemVm.selectedDrink = selectedDrink;

    function getProductImageUrl() {
      return '/assets/images/' + productItemVm.drink.image;
    }
    
    function selectedDrink(drinkItem) {
      productItemVm.onSelectedDrink({selectedDrink: drinkItem});
    }
  }
})();
