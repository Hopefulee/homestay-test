describe('Product item component', function () {
  var component, rootScope;

  beforeEach(module('vendingMachine'));
  beforeEach(function () {
    module(function () {
    });

    inject(function ($injector, _$componentController_) {
      rootScope = $injector.get('$rootScope');

      component = _$componentController_('productItem',
        {'$scope': rootScope.$new()},
        {
          drink: { quantity: 10, title: 'drink 1', image: 'image.png' },
          onSelectedDrink: jasmine.createSpy('onSelectedDrink')
        }
      );
    });
  });

  describe('Product image url', function () {
    beforeEach(function () {
      component.drink = { quantity: 10, title: 'drink 1', image: 'image.png' };
    });
    it('should have correct image path', function () {
      expect(component.productImage).toEqual('/assets/images/' + component.drink.image)
    });
  });

  describe('Select drink', function () {
    var drinkItem = { quantity: 10, title: 'drink 1'};
    beforeEach(function () {
      component.selectedDrink(drinkItem);
    });
    it('should call purchaseByCash', function () {
      expect(component.onSelectedDrink).toHaveBeenCalledWith({selectedDrink: drinkItem});
    });
  });
});
