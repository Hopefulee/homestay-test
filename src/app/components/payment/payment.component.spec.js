describe('Payment component', function () {
  var component, rootScope;

  beforeEach(module('vendingMachine'));
  beforeEach(function () {
    module(function () {
    });

    inject(function ($injector, _$componentController_) {
      rootScope = $injector.get('$rootScope');

      component = _$componentController_('payment',
        {'$scope': rootScope.$new()},
        {
          selectedDrink: { quantity: 10, title: 'drink 1' },
          purchaseByCash: jasmine.createSpy('purchaseByCash'),
          purchaseByCreditCard: jasmine.createSpy('purchaseByCreditCard')
        }
      );
    });
  });

  describe('Purchase by cash', function () {
    beforeEach(function () {
      component.selectedDrink = { quantity: 10, title: 'drink 1' };
      component.onPurchaseByCash();
    });
    it('should call purchaseByCash', function () {
      expect(component.purchaseByCash).toHaveBeenCalledWith({selectedDrink: component.selectedDrink});
    });
  });

  describe('Purchase by credit card', function () {
    beforeEach(function () {
      component.selectedDrink = { quantity: 10, title: 'drink 1' };
      component.onPurchaseByCreditCard();
    });
    it('should call purchaseByCash', function () {
      expect(component.purchaseByCreditCard).toHaveBeenCalledWith({selectedDrink: component.selectedDrink});
    });
  });
});
