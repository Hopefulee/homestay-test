(function () {
  'use strict';

  angular.module('vendingMachine').component('payment', {
    templateUrl: 'app/components/payment/payment.component.html',
    controller: Payment,
    controllerAs: 'paymentVm',
    bindings: {
      selectedDrink: '<',
      locked: '<',
      cash: '<',
      creditCard: '<',
      soldCan: '<',
      restockQuantity: '<',
      purchaseByCash: '&',
      purchaseByCreditCard: '&'
    }
  });

  Payment.$inject = [];

  function Payment() {
    var paymentVm = this;
    paymentVm.onPurchaseByCash = onPurchaseByCash;
    paymentVm.onPurchaseByCreditCard = onPurchaseByCreditCard;

    function onPurchaseByCash() {
      if (paymentVm.selectedDrink) {
        paymentVm.purchaseByCash({selectedDrink: paymentVm.selectedDrink});
      }
    }

    function onPurchaseByCreditCard() {
      if (paymentVm.selectedDrink) {
        paymentVm.purchaseByCreditCard({selectedDrink: paymentVm.selectedDrink});
      }
    }
  }
})();
