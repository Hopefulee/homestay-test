(function() {
  'use strict';

	describe('drink service', function () {
    beforeEach(module('vendingMachine'));
    beforeEach(function () {
      inject(function () {
      });
    });

    it('should return 10 different type of drinks', inject(function (drinkService) {
      var data = drinkService.getDrinks();
      expect(data.length).toEqual(10);
    }));
	});
})();
