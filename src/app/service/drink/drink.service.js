(function() {
  'use strict';

  angular.module('vendingMachine')
      .service('drinkService', drinkService);

  /** @ngInject */
  function drinkService() {
    var data = [
      {
        title: 'Coke',
        description: 'Coke',
        price: 1.50,
        quantity: 10,
        image: 'coke.jpg'
      },
      {
        title: 'Coke Zero',
        description: 'Coke Zero',
        price: 1.50,
        quantity: 10,
        image: 'coke-zero.jpg'
      },
      {
        title: 'Diet Coke',
        description: 'Diet Coke',
        price: 1.50,
        quantity: 10,
        image: 'diet-coke.jpg'
      },
      {
        title: 'Sprite',
        description: 'Sprite',
        price: 1.50,
        quantity: 10,
        image: 'sprite.png'
      },
      {
        title: 'Fanta',
        description: 'Fanta',
        price: 1.50,
        quantity: 10,
        image: 'fanta.jpg'
      },
      {
        title: 'Pepsi',
        description: 'Pepsi',
        price: 1.30,
        quantity: 10,
        image: 'pepsi.jpg'
      },
      {
        title: 'Pepsi Max',
        description: 'Pepsi Max',
        price: 1.30,
        quantity: 10,
        image: 'pepsi-max.jpg'
      },
      {
        title: 'Lift',
        description: 'Lift',
        price: 1.30,
        quantity: 10,
        image: 'lift.jpg'
      },
      {
        title: '7 UP',
        description: '7 UP',
        price: 1.30,
        quantity: 10,
        image: '7-up.jpg'
      },
      {
        title: 'Solo',
        description: 'Solo',
        price: 1.30,
        quantity: 10,
        image: 'solo.jpg'
      }
    ];

    this.getDrinks = getDrinks;

    function getDrinks() {
      return data;
    }
  }

})();
