(function() {
  'use strict';

  angular
    .module('vendingMachine', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngRoute']);

})();
