/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('vendingMachine')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
