(function() {
  'use strict';

  angular
    .module('vendingMachine')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
  }

})();
