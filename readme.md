# Vending Machine
The app is responsive app. You can reduce the size of the browser to view the responsiveness in different size.

## Installation
* install npm and bower 
* use node version `6.12.3`
* type `nmp install` then `bower install`
* type `gulp serve` to run the application
* type `gulp test` to run the test
* Try on [youtube demo](https://www.youtube.com/watch?v=EjXgNE5jkbE&t=4s) page.

## Rules and Assumption
*  The data will only be kept in run time, refresh the browser will reset everything again.
*  Customer cannot purchase the drink if the quantity of drink is dropped to 0.
*  There is a lock icon on the right hand side of the ven vending machine. Click on it to open the machine, click it again to close the machine.
*  Customers cannot purchase any drink when the machine is opened. 
*  Once the vending machine is opened, you will be able to see the current stock level of each drinks, money collected from cash and credit card, and sold cans and previous number of cans added from last restock.
*  Each time for the restock, it will refill all the cans to quantity to 10.

